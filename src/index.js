import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react'
import App from './pages/App';
import * as serviceWorker from './serviceWorker';
import './lib/fa';
import stores from './stores';
import './index.scss';

const Root = props => (
	<Provider {...stores}>
		<App />
	</Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
