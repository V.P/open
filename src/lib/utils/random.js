export function validateEmail(email) {
    var re = /[^\s@]+@[^\s@]+\.[^\s@]+/;
    return re.test(String(email).toLowerCase());
}

export function socketUrl(path) {
    return `${process.env.REACT_APP_WS_URL}${path}`;
}



export function truncate(content, limit, after) {

    // Convert the content into an array of words
    // Remove any words above the limit
    content = content.split(' ').slice(0, limit);

    // Convert the array of words back into a string
    // If there's content to add after it, add it
    content = content.join(' ') + (after ? after : '');

    // Inject the content back into the DOM
    return content
};