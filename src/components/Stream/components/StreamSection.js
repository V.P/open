import React from 'react';
import StreamDateHeader from "./StreamDateHeader";
import { Media, Level } from '../../../vendor/bulma';
import Avatar from '../../Avatar';
import FullName from '../../FullName';
import Streak from '../../Streak';
import Tda from '../../Tda';
import Task from '../../Task';
import './StreamSection.scss';


const StreamPart = ({ userActivity }) => {
    const user = userActivity[0].user;

    return (
        <div className="StreamPart">
            <Media>
                <Media.Left>
                    <Avatar is={24} user={user} />
                </Media.Left>
                <Media.Content>
                    <small className="name"><FullName user={user} /></small><br />
                    <Level mobile class="badges">
                        <Level.Left>
                            <Level.Item>
                                <Streak days={user.streak} />
                            </Level.Item>
                            <Level.Item>
                                <Tda days={user.tda} />
                            </Level.Item>
                        </Level.Left>
                    </Level>
                </Media.Content>
            </Media>
            <div className="activity">
                {userActivity.map(task => <Task task={task} key={task.id} />)}
            </div>
        </div>
    )
}

class StreamSection extends React.Component {
    render() {
        let activityData = this.props.activityData;
        let date = this.props.date;

        return (
            <section className="StreamSection">
                <StreamDateHeader
                    date={date}
                    position={this.props.position}
                    canSwitchType={this.props.canSwitchType}
                    isFollowingFeed={this.props.isFollowingFeed}
                    onSwitch={this.props.onSwitch}
                />

                {Object.keys(activityData).map(
                    (key) => {
                        return <StreamPart key={activityData[key][0].id} userActivity={activityData[key]} />
                    }
                )}
            </section>
        )
    }
}

StreamSection.propTypes = {}

export default StreamSection;
