import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '../../vendor/bulma';
import InfiniteScroll from 'react-infinite-scroll-component';
import StreamSection from './components/StreamSection';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { sortStreamByActivity } from '../../lib/utils/tasks';
import Spinner from "../Spinner";

/**
 * Stream Component
 * @param tasks tasks to be displayed.
 * @param loadMore called on loadmore click or scroll.
 * @param isSyncing boolean indicating loading status.
 * @param hasMore boolean indicating if there is data to be loaded on server.
 */

class Stream extends React.Component {
    render() {
        const data = sortStreamByActivity(this.props.tasks);

        if (Object.keys(data).length === 0 && !this.props.hasMore && !this.props.isSyncing) {
            return this.props.noActivityComponent
        }

        return (
            <InfiniteScroll
                next={this.props.loadMore}
                hasMore={this.props.hasMore}
                style={{ overflow: 'none'}}>
                {Object.keys(data).map(
                    (date) => <StreamSection
                        key={date}
                        position={Object.keys(data).indexOf(date)}
                        canSwitchType={this.props.canSwitchType}
                        isFollowingFeed={this.props.isFollowingFeed}
                        onSwitch={this.props.onSwitch}
                        date={date}
                        activityData={data[date]} />
                )}

                {this.props.hasMore &&
                    <center>
                        <Button loading={this.props.isSyncing} className={"is-rounded"} onClick={this.props.loadMore}>
                            <FontAwesomeIcon icon={'arrow-circle-down'} /> &nbsp; Load more tasks...
                        </Button>
                    </center>
                }
                {!this.props.hasMore && this.props.isSyncing && <Spinner />}
                {!this.props.hasMore && !this.props.isSyncing && <div>That's it!</div>}
            </InfiniteScroll>
        )
    }
}

Stream.propTypes = {
    tasks: PropTypes.array.isRequired,
    loadMore: PropTypes.func.isRequired,
    isSyncing: PropTypes.bool.isRequired,
    hasMore: PropTypes.bool.isRequired,
}

Stream.defaultProps = {
    noActivityComponent: <div>Nothing here.</div>,
    canSwitchType: false,
    isFollowingFeed: false,
    onSwitch: null,
}

export default Stream;
