import React from 'react';
import PropTypes from 'prop-types';

const FullName = ({ user, prependUsername=false }) => (
    <span>{user.first_name ? `${user.first_name} ${user.last_name}` : (prependUsername ? `@${user.username}` : user.username)}</span>
)

FullName.propTypes = {
    user: PropTypes.object.isRequired,
    prependUsername: PropTypes.bool,
}

export default FullName;