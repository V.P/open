import React from 'react';
import PropTypes from 'prop-types';

class Avatar extends React.Component {

    getClassNames = () => {
        let classNames = "Avatar image img-circle is-square";

        if (this.props.is) {
            classNames += ` is-${this.props.is}x${this.props.is}`;
        }

        if (this.props.withBadge) {
            classNames += ' badge is-badge-white is-avatar-badge is-badge-small';
        }

        if (this.props.className) {
            classNames += ` ${this.props.className}`
        }

        return classNames
    }
    render() {
        return (
            <span
                style={{paddingTop: 0}}
                data-badge={this.props.user && this.props.user.streak ? `🔥 ${this.props.user.streak}` : null}
                className={`${this.getClassNames()}`}>
                <img
                    alt={this.props.user.username}
                    src={this.props.user.avatar || this.props.src} />
            </span>
        )
    }
}

Avatar.defaultProps = {
    is: 48,
    className: '',
}

Avatar.propTypes = {
    is: PropTypes.number,
    className: PropTypes.string,
}

export default Avatar;