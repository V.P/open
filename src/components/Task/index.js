import React from 'react';
import Emoji from 'components/Emoji';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {getAppIcon} from "lib/apps";
import Linkify from 'react-linkify';
import {startsWith} from "lodash-es";
import {Image} from "vendor/bulma";
import {Lightbox} from 'react-modal-image';
import {processTaskString} from "lib/utils/tasks";
import {Button, Icon} from "vendor/bulma";

import './index.scss';

function findWord(word, str) {
    return str.split(/\s+|\./) // split words based on whitespace or a '.'
        .includes(word)
}

function isProductLaunch(task) {
    // Tests for "launched #launched", #launched, or "launched".
    return (
        (findWord('launched', task.content.toLowerCase())
            && !task.content.toLowerCase().includes("#launched")
            && task.project_set.length > 0)
        ||
        (findWord('launched', task.content.toLowerCase())
            && task.content.toLowerCase().includes("#launched")
            && task.project_set.length > 0)
    )
}

/*

function isRecentTask(task) {
    const LAUNCH_HOUR_THRESHOLD = 60 * 60 * 1000;
    return ((new Date) - (Date.parse(task.done_at))) < LAUNCH_HOUR_THRESHOLD
}

*/


class Task extends React.Component {
    state = {
        attachmentOpen: false,
        detailsOpen: false,
        hover: false,
    }

    onMouseEnter = e => {
        this.setState({
            hover: true,
        })
    }

    onMouseLeave = e => {
        this.setState({
            hover: false,
        })
    }

    toggleAttachment = () => {
        this.setState({ attachmentOpen: !this.state.attachmentOpen })
    }

    toggleDetails = () => {
        this.setState({ detailsOpen: !this.state.detailsOpen })
    }

    getClassNames = () => {
        let baseClass = 'Task';
        const task = this.props.task;

        if (task.done) {
            baseClass += " done"
        } else if (task.in_progress) {
            baseClass += " in-progress";
        } else {
            baseClass += " remaining"
        }

        if (isProductLaunch(this.props.task)) {
            baseClass += " launch"
        }

        return baseClass
    }

    renderIcon = () => {
        let doneIcon = 'check-circle';
        let remainingIcon = 'dot-circle';
        let doneColor = "#27ae60"
        let remainingColor = "#f39c12"

        if (this.props.task.event) {
            doneIcon = getAppIcon(this.props.task.event);
            remainingIcon = getAppIcon(this.props.task.event);
        }

        if (isProductLaunch(this.props.task)) {
            return <Emoji emoji={"🚀"} />
        }

        return (this.props.task.done) ?
            <FontAwesomeIcon icon={doneIcon} color={doneColor} />
            : <FontAwesomeIcon icon={remainingIcon} color={remainingColor} />
    }

    renderContent = () => {
        let task = processTaskString(this.props.task);
        if (this.props.contentWrapper) {
            let Wrapper = this.props.contentWrapper;
            task = <Wrapper>{task}</Wrapper>
        }
        if (this.props.withDetailModal) {
            task = <span onClick={this.toggleDetails}>{task}</span>
        }
        task = <Linkify properties={{'target': '_blank', 'rel': 'nofollow noopener noreferrer'}}>{task}</Linkify>
        if (this.props.withTooltip) {
            // task = <HelpTooltip>{task}</HelpTooltip>
        }
        return task
    }

    renderAttachments = () => {
        if (this.props.task.attachment && startsWith(this.props.task.attachment, 'http')) {
            return (
                <div>
                    <Image onClick={this.toggleAttachment} className={"attachment"} src={this.props.task.attachment} />
                    {
                        this.state.attachmentOpen && (
                            <Lightbox
                                small={this.props.task.attachment}
                                large={this.props.task.attachment}
                                alt={this.props.task.content}
                                onClose={this.toggleAttachment}
                            />
                        )
                    }
                </div>
            )
        } else {
            return null
        }
    }

    renderCounts = () => {
        return (
            <span>
                {this.props.task.comment_count > 0 ? <span style={{display: "inline-block"}} className={"PraiseIndicator has-text-grey-light"}><Emoji emoji={"💬"}/>{this.props.task.comment_count}</span> : null}
            </span>
        )
    }

    renderExtras = () => {
        
    }

    render() {
        if (!this.props.task) {
            return null;
        }

        return (
            <div
                onMouseEnter={this.onMouseEnter}
                onMouseLeave={this.onMouseLeave}
                className={this.getClassNames()}>
                {this.renderIcon()} <span className={"task-content"}>{this.renderContent()}</span> {this.renderCounts()}
                {this.renderAttachments()}
                {this.renderExtras()}
            </div>
        )
    }
}

Task.propTypes = {
    task: PropTypes.object
}

Task.defaultProps = {
    withDetailModal: true,
    withTooltip: true,
    withCounts: true,
    withPraise: true,
}

export default Task;
