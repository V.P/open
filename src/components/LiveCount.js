import React from 'react';
import ReconnectingWebSocket from "reconnecting-websocket/dist/reconnecting-websocket";

class LiveCount extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            count: this.props.initialCount,
        }
    }

    componentDidMount() {
        this.connect();
    }

    componentWillUnmount() {
        this.disconnect()
    }

    connect = () => {
        const indexUrl = 'wss://api.getmakerlog.com/explore/stream/';
        this.socket = new ReconnectingWebSocket(indexUrl);
        this.socket.onopen = () => {
            console.log(`Makerlog/LiveCount: Established connection to ${indexUrl}.`)
        }
        this.socket.onmessage = this.onWsEvent
        this.socket.onclose = () => {
            console.log(`Makerlog/LiveCount: Closed connection to ${indexUrl}.`)
        }
    }

    onWsEvent = (event) => {
        const data = JSON.parse(event.data)
        switch (data.type) {
            case 'task.created':
                this.setState({
                    count: this.state.count + 1
                });
                break;

            case 'task.deleted':
                this.setState({
                    count: this.state.count - 1
                });
                break;

            default:
                return;
        }
    }

    disconnect = () => {
        if (this.socket) {
            this.socket.close()
        }
    }

    render() {
        let Component = this.props.component;

        return <Component count={this.state.count} />
    }
}

export default LiveCount;
