import React from 'react';
import styled from 'styled-components';

const HeaderTrend = styled.div`
    position: absolute;
    display: flex;
    flex-direction: row;
    width: 100%;
    height: 100%;
`

const Bar = styled.span`
        background-color: ${props => props.color};
        height: 50px;
        margin-right: 2px;
        flex-grow: 1;
        max-height: 90%;
        align-self: flex-end;
        height: ${props => props.data * 5}%
`

export default ({ trend, color }) => {
    if (!trend) return null;

    return (
        <HeaderTrend>
            {trend.map(
                point => <Bar color={color} data={point} />
            )}
        </HeaderTrend>
    )
}